<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

class LancementController extends AbstractController
{
    #[Route('/lancement', name: 'app_lancement')]
    public function index(): Response
    {
        return $this->render('lancement/index.html.twig', [
            'controller_name' => 'LancementController',
        ]);
    }
}

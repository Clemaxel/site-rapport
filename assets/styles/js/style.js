function shrinkImage(columnText) {
    var image = document.querySelector('.column-img img');
    image.style.transform = "scale(0.9)";
}

function resetImage(columnText) {
    var image = document.querySelector('.column-img img');
    image.style.transform = "scale(1)";
}